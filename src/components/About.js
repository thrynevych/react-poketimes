import React from 'react';
import Rainbow from '../hoc/Rainbow';

const About = () => {
    return (
        <div className="container">
            <h4 className="Center">About</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci aliquid at distinctio dolorem doloremque dolores fuga ipsa laboriosam molestiae numquam omnis qui quibusdam quos recusandae sapiente sequi, soluta? Dolores?</p>
        </div>
    )
};

export default Rainbow(About);