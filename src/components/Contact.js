import React from 'react';

const Contact = () => {
    return (
        <div className="container">
            <h4 className="Center">Contact</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci aliquid at distinctio dolorem doloremque dolores fuga ipsa laboriosam molestiae numquam omnis qui quibusdam quos recusandae sapiente sequi, soluta? Dolores?</p>
        </div>
    )
};

export default Contact;